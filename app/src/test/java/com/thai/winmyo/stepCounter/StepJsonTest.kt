package com.thai.winmyo.stepCounter

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.google.gson.JsonSyntaxException
import org.json.JSONArray
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import java.util.*

@RunWith(RobolectricTestRunner::class)
class StepJsonTest {

    @Test
    fun testRawData() {
        val list = getAppContext().getTwoDimensionListFromJSON(R.raw.step_data_10)
        AlgoStep(list).display()
    }

    @Test
    fun testMockData() {
        val list = mutableListOf<List<Double>>()
        for (i in 1..3) {
            val rowList = mutableListOf<Double>()
            for (j in 1..100) {
                if (i == 1) {
                    rowList.add(j.toDouble())
                } else {
                    rowList.add(Random().nextDouble())
                }
            }
            list.add(rowList)
        }
        AlgoStep(list).display()
    }

    private fun getAppContext() = ApplicationProvider.getApplicationContext<StepApp>()
}

class AlgoStep(private val twoDimenList: List<List<Double>>) {

    private fun getTransportList() = twoDimenList.getTransposeList()

    fun getSortedOneDimenList(): Map<Int, Double> {
        return getTransportList().mapIndexed { index, list -> index to list.average() }.toMap()
    }

    fun getDivisorList(): List<Int> {
        return getSelfNotIncluded(getSortedOneDimenList().size)
    }

    fun getQuotientList(): List<Int> {
        return getDivisorList().map { getSortedOneDimenList().size / it }
    }

    fun getQuotientPrimeNumberDivisorList(): List<List<Int>> {
        return getQuotientList().map { quotient -> getPrimeNumberDivisor(quotient) }
    }

    fun getPValue(): List<Int> {
        val quotient = getQuotientList()
        return getQuotientPrimeNumberDivisorList().mapIndexed { index, quotientList -> pValue(quotient[index], quotientList) }
    }

    fun getDPValueList(): List<Pair<Int, Int>> {
        val divisor = getDivisorList()
        val pValue = getPValue()
        return pValue.zip(divisor)
    }

    fun getNewSet(): List<Int> {
        return getDPValueList().map { pair ->
            getNumberList(pair.first).map { it * pair.second }
        }.flatten().distinct().sorted()
    }

    fun display() {
        println("algo getTransportList ${getTransportList()}")
        println("algo list size column " + getTransportList().size + " row " + getTransportList().getOrNull(0)?.size)
        println("algo getSortedOneDimenList ${getSortedOneDimenList()}")
        println("algo getDivisorList ${getDivisorList()}")
        println("algo getQuotientList ${getQuotientList()}")
        println("algo getQuotientPrimeNumberDivisorList ${getQuotientPrimeNumberDivisorList()}")
        println("algo getPValue ${getPValue()}")
        println("algo getDPValueList ${getDPValueList()}")
        val newSet = getNewSet()
        val newSetMap = newSet.map { it to getTransportList()[it - 1] }.toMap().map { "\n ${it.key} => ${it.value} " }
        println("algo newSetMap $newSetMap")

    }
}

fun List<List<Double>>.getTransposeList(): List<List<Double>> {
    val row = size
    val column = this[0].size
    val transpose = Array(column) { DoubleArray(row) }
    for (i in 0 until row) {
        for (j in 0 until column) {
            transpose[j][i] = this[i][j]
        }
    }
    return transpose.map { it.toList() }
}

fun Context.getTwoDimensionListFromJSON(rawId: Int): List<List<Double>> {
    val listdata = mutableListOf<List<Double>>()
    try {
        val array = JSONArray(readTextFile(resources.openRawResource(rawId)))

        for (i in 0 until array.length()) {
            listdata.add(array.getJSONArray(i).toDoubleList())
        }
    } catch (e: JsonSyntaxException) {

    }
    return listdata
}

fun JSONArray.toDoubleList(): List<Double> {
    val listdata = mutableListOf<Double>()
    for (i in 0 until length()) {
        if (getString(i).isNotBlank())
            listdata.add(getString(i).toDouble())
    }
    return listdata
}

fun readTextFile(inputStream: InputStream): String {
    val outputStream = ByteArrayOutputStream()

    val buf = ByteArray(1024)
    var len = 0
    try {
        while (len != -1) {
            outputStream.write(buf, 0, len)
            len = inputStream.read(buf)
        }
        outputStream.close()
        inputStream.close()
    } catch (e: IOException) {

    }

    return outputStream.toString()
}