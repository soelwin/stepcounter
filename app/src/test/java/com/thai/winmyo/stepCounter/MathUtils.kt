package com.thai.winmyo.stepCounter

import kotlin.math.roundToInt


fun getDivisors(n: Int): List<Int> {
    val list = mutableListOf<Int>()
    for (i in 1..n)
        if (n % i == 0)
            list.add(i)
    return list.filter { it != 1 }
}

fun getPrimeNumberDivisor(n: Int): List<Int> {
    return getDivisors(n).filter { isPrime(it) }
}

fun getSelfNotIncluded(n: Int): List<Int> {
    return if (isPrime(n)) listOf(1) else getDivisors(n).filter { it != n }
}

fun isPrime(num: Int): Boolean {
    var i = 2
    var flag = false
    while (i <= num / 2) {
        if (num % i == 0) {
            flag = true
            break
        }
        ++i
    }
    return !flag
}

fun getNumberList(n: Int): List<Int> {
    val list = mutableListOf<Int>()
    for (i in 1..n) {
        list.add(i)
    }
    return list
}

fun pValue(quotient: Int, primeDivisorList: List<Int>): Int {
    val list =
            listOf(quotient.toDouble())
                    .plus(
                            primeDivisorList.map { pi ->
                                1.0 - (1.0 / pi.toDouble())
                            }
                    )
    return list.multiple()
}

public fun List<Double>.multiple(): Int {
    return this.reduce { acc, d -> acc * d }.roundToInt()
}