package com.thai.winmyo.stepCounter

import com.thai.winmyo.stepCounter.data.CheckActivityData
import com.thai.winmyo.stepCounter.data.PersonActivity
import com.thai.winmyo.stepCounter.data.calculateActivity
import junit.framework.Assert
import org.junit.Test


class PersonActivityTest {
    @Test
    fun calculateActivityTest() {
        Assert.assertEquals(PersonActivity.Laying,calculateActivity(
                CheckActivityData(0, 7, 0, 0)
        ))
        Assert.assertEquals(PersonActivity.Standing,calculateActivity(
                CheckActivityData(0, 15, 0, 0)
        ))
        Assert.assertEquals(PersonActivity.NormalWalk,calculateActivity(
                CheckActivityData(10, 10, 7, 0)
        ))
        Assert.assertEquals(PersonActivity.StandWalk,calculateActivity(
                CheckActivityData(10, 10, 10, 5)
        ))
        Assert.assertEquals(PersonActivity.Jogging,calculateActivity(
                CheckActivityData(1, 10, 8, 10)
        ))
        Assert.assertEquals(PersonActivity.Downstairs,calculateActivity(
                CheckActivityData(10, 10, 90, 10)
        ))
        Assert.assertEquals(PersonActivity.Upstairs,calculateActivity(
                CheckActivityData(1, 10, 25, 10)
        ))
    }
}