package com.thai.winmyo.stepCounter

import com.thai.winmyo.stepCounter.data.*
import org.junit.Assert
import org.junit.Test

class CombineValueTest {
    private val acList = listOf(1.0f, 2.0f, 3.0f, 4.0f, 5.0f)
    private val mgList = listOf(1.0f, 2.0f, 3.0f, 4.0f, 5.0f)
    private val combineList = mutableListOf<Float>()
    @Test
    @Throws(Exception::class)
    fun testCombine() {
        assert(acList.isSizeEqual(mgList))
        for (ac in acList.withIndex()) {
            val i = ac.index

            val acI = ac.value.toDouble()
            val mgI = mgList[i].toDouble()
            val vaI = acList.getVariance(i)
            val vbI = acList.getVariance(i)

            combineList.add(calculateCombineFormula(acI, mgI, vaI, vbI).toFloat())
        }
        Assert.assertEquals(acList.size, combineList.size)
    }

    @Test
    fun calculateMeanTest() {
        Assert.assertEquals(3.0, acList.calculateMean(), 0.0)
    }

    @Test
    fun divideBySampleTest() {
        Assert.assertEquals(1.0,2.0.divideBySample(3),0.0)
        Assert.assertEquals(0.5,2.0.divideBySample(5),0.0)
    }

    @Test
    fun getSigmaTest(){
        Assert.assertEquals(1.0,acList.getSigma(0),0.0)
        Assert.assertEquals(3.0,acList.getSigma(1),0.0)
        Assert.assertEquals(6.0,acList.getSigma(2),0.0)
        Assert.assertEquals(10.0,acList.getSigma(3),0.0)
        Assert.assertEquals(15.0,acList.getSigma(4),0.0)
    }

    @Test
    fun getVarianceTest(){
        Assert.assertEquals(0.25,acList.getVariance(0),0.0)
        Assert.assertEquals(0.75,acList.getVariance(1),0.0)
        Assert.assertEquals(1.5,acList.getVariance(2),0.0)
        Assert.assertEquals(2.5,acList.getVariance(3),0.0)
        Assert.assertEquals(3.75,acList.getVariance(4),0.0)
    }

    @Test
    fun combineVarianceTest(){
        Assert.assertEquals(0.6,6.0.combineVariance(4.0),0.0)
        Assert.assertEquals(0.25,10.0.combineVariance(30.0),0.0)
    }
}
