package com.thai.winmyo.stepCounter;

import com.thai.winmyo.stepCounter.data.SensorPoint;
import com.thai.winmyo.stepCounter.util.CommonHelper;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void testPeak() {
        List<Double> avList = getFakeData();

        Double next = null, prev = null, current;
        int peakCount = 0, peakOccurrence = 0;
        for (int i = 0; i < avList.size(); i++) {
            current = avList.get(i);
            if (i - 1 > 0)
                prev = avList.get(i - 1);
            if (i + 1 < avList.size())
                next = avList.get(i + 1);

            if (next != null && prev != null && current != null && (current > next && current > prev)) {
                System.out.println("Peak Occurrence " + peakOccurrence);
                if (peakOccurrence > 3)
                    peakCount++;
                peakOccurrence = 0;
                System.out.println("Peak " + peakCount);
            } else {
                peakOccurrence++;
            }
            next = prev = null;
        }
        System.out.println("Peak Count " + peakCount);

    }

    private List<Double> getFakeData() {
        List<Double> avList = new ArrayList<>();
        avList.add(1.0);
        avList.add(2.0);
        avList.add(3.0);
        avList.add(4.0);
        avList.add(3.0);

        avList.add(1.0);
        avList.add(2.0);
        avList.add(3.0);
        avList.add(4.0);
        avList.add(3.0);

        avList.add(1.0);
        avList.add(2.0);
        avList.add(3.0);
        avList.add(4.0);
        avList.add(3.0);

        return avList;
    }

    @Test
    public void testCeil() {
        System.out.printf("\n %12s %12s %12s %12s", "x", "y", "z", "z");
        System.out.printf("\n %12.6f %12.6f %12.6f %12d", 1.0f, 2.0f, 3.0f, 7);
        System.out.printf("\n %12.6f %12.6f %12.6f %12d", -11.0f, 2.0f, -3.0f, 7);
        System.out.printf("\n %12.6f %12.6f %12.6f %12d", 1.0f, 2.0f, 30.0f, 700);
        System.out.printf("\n %12.6f %12.6f %12.6f %12d", 1.0f, 2.0f, 333.0f, 7);

        System.out.println();
        System.out.printf("|%10.6f|", 93.0f);
        System.out.println();
        System.out.printf("|%12s|", "Soe");

        System.out.print(System.currentTimeMillis());
    }

    @Test
    public void testFormat() {
        String temp = String.format(Locale.ENGLISH, "%.6f", 22222.0000134567);
        assertEquals(temp, "22222.000013");
    }

    @Test
    public void testerGson() {
        List<SensorPoint> sensorPoints = new ArrayList<>();
        sensorPoints.add(new SensorPoint(10, 10, 10, System.currentTimeMillis(), 100));
        sensorPoints.add(new SensorPoint(10, 10, 10, System.currentTimeMillis(), 100));
        sensorPoints.add(new SensorPoint(10, 10, 10, System.currentTimeMillis(), 100));

    }
}