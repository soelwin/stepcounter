package com.thai.winmyo.stepCounter.plot

/**
 * Created by sherly on 1/17/18.
 */


enum class StepSensor {
    AC, GY, MG
}
