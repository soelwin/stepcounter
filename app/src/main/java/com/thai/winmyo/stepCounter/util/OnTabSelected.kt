package com.thai.winmyo.stepCounter.util

import android.support.design.widget.TabLayout

/**
 * Created by sherly on 3/6/18.
 */

interface OnTabSelected : TabLayout.OnTabSelectedListener {

    override fun onTabUnselected(tab: TabLayout.Tab) {

    }

    override fun onTabReselected(tab: TabLayout.Tab) {

    }
}
