package com.thai.winmyo.stepCounter.util

import android.os.Environment
import android.text.Editable
import android.text.TextWatcher
import android.util.SparseArray
import android.view.View
import android.widget.EditText
import com.google.gson.GsonBuilder
import com.thai.winmyo.stepCounter.data.SaveFile
import com.thai.winmyo.stepCounter.data.SensorPoint
import com.thai.winmyo.stepCounter.data.buildFileName
import timber.log.Timber
import java.io.*
import java.text.DecimalFormat
import java.util.*

/**
 * Created by soelwin on 7/24/16.
 */
object CommonHelper {

    fun round(value: Double, precision: Int): Double {
        val scale = Math.pow(10.0, precision.toDouble()).toInt()
        return Math.round(value * scale).toDouble() / scale
    }

    private fun storeBitmapExternal(fileName: String): File {
        val mediaStorageDir = File(Environment.getExternalStorageDirectory().toString() + "/WinStepCounter/")
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                throw IllegalStateException()
            }
        }
        val file = File(mediaStorageDir, fileName)
        try {
            if (file.exists() && !file.delete()) {
                throw IllegalStateException()
            }
            if (!file.createNewFile()) {
                throw IllegalStateException()
            }
        } catch (e: IOException) {
            Timber.d(e.localizedMessage)
        }

        return file
    }

    fun doSaving(saveFile: SaveFile) {
        val fileStream: FileOutputStream
        val myOutWriter: OutputStreamWriter
        val myFile = storeBitmapExternal(saveFile.buildFileName())
        try {
            fileStream = FileOutputStream(myFile, true)
            myOutWriter = OutputStreamWriter(fileStream)
            val saveFileInText = GsonBuilder().create().toJson(saveFile)

            myOutWriter.append(saveFileInText)
            myOutWriter.flush()

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun <T> partitionList(list: List<T>, L: Int): List<List<T>> {
        val parts = ArrayList<List<T>>()
        val N = list.size
        var i = 0
        while (i < N) {
            parts.add(ArrayList(list.subList(i, Math.min(N, i + L))))
            i += L
        }
        return parts
    }

    fun isValidTime(diff: Long): Boolean {
        val recordingInterval = SettingPref.retrieveRecordingInterval()
        return diff >= recordingInterval
    }

    fun getAverageValue(sensorPoint: SensorPoint): Double {
        return Math.sqrt(Math.pow(sensorPoint.x.toDouble(), 2.0) + Math.pow(sensorPoint.y.toDouble(), 2.0) + Math.pow(sensorPoint.z.toDouble(), 2.0))
    }

    fun getRoundAverageValue(sensorPoint: SensorPoint): Double {
        return round(getAverageValue(sensorPoint), 1)
    }

    fun getDisplacement(sensorPoint: SensorPoint): Double {
        //return 1.5 * getAverageValue(sensorPoint) * Math.pow(sensorPoint.getDiffTime(), 2) * Math.pow(10, -6);
        return getAverageValue(sensorPoint) * Math.pow(10.0, -2.0)

    }

    fun convertToSaveFile(filePath: String): SaveFile {
        var file = SaveFile()
        try {
            val content = CommonHelper.readFile(filePath)
            file = GsonBuilder().create().fromJson<SaveFile>(content, SaveFile::class.java)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return file
    }

    @Throws(IOException::class)
    fun readFile(fileName: String): String {
        BufferedReader(FileReader(fileName)).use { br ->
            val sb = StringBuilder()
            var line: String? = br.readLine()

            while (line != null) {
                sb.append(line)
                sb.append("\n")
                line = br.readLine()
            }
            return sb.toString()
        }
    }

    fun frequency(list: List<Double>, value: Double): Int {
        var result = 0
        for (item in list) {
            if (formatValue(value) == formatValue(item)) {
                result++
            }
        }
        return result
    }

    fun convertToList(sparseArray: SparseArray<List<SensorPoint>>?): List<SensorPoint>? {
        if (sparseArray == null) return null
        val arrayList = ArrayList<SensorPoint>()
        for (i in 0 until sparseArray.size())
            arrayList.addAll(sparseArray.valueAt(i))
        return arrayList
    }

    fun getPercentage(total: Double, amount: Double): Int {
        if (total == 0.0) {
            return 0
        }
        val percentage = Math.round(amount * 100.0f / total).toDouble()
        return percentage.toInt()
    }

    private fun formatValue(value: Double): Double {
        val df = DecimalFormat("#.#")
        return java.lang.Double.valueOf(df.format(value))!!
    }

    fun getPointsByGroup(sensorPoints: List<SensorPoint>, grouping: Int): List<List<SensorPoint>> {
        return CommonHelper.partitionList(sensorPoints, grouping)
    }

}
fun View.onClick(cb: (View) -> Unit) = this.setOnClickListener { cb.invoke(it) }

fun EditText.onTextChanged(action: (CharSequence) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(string: Editable?) = Unit
        override fun beforeTextChanged(string: CharSequence?, start: Int, count: Int, after: Int) = Unit
        override fun onTextChanged(string: CharSequence?, start: Int, before: Int, count: Int) {
            action(string ?: "")
        }
    })
}