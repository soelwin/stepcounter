package com.thai.winmyo.stepCounter.plot

import java.util.ArrayList

/**
 * Created by sherly on 1/17/18.
 */

class AppXYDataSource {
    private var mYValue: MutableList<Double>? = null

    val itemCount: Int
        get() = mYValue!!.size

    init {
        buildData()
    }

    private fun buildData() {
        mYValue = ArrayList()
        for (i in 0 until X_COLUMN_COUNT)
            mYValue!!.add(0.0)
    }

    private fun removeFirstIfValid() {
        if (mYValue!!.size == X_COLUMN_COUNT)
            mYValue!!.removeAt(0)
    }

    fun resetData() {
        mYValue!!.clear()
        buildData()
    }

    fun appendDataToLast(value: Double) {
        removeFirstIfValid()
        mYValue!!.add(value)
    }

    fun getX(index: Int): Int {
        return index
    }

    fun getY(index: Int): Double {
        return mYValue!![index]
    }

    companion object {
        private const val X_COLUMN_COUNT = 41// TODO :
    }

}
