package com.thai.winmyo.stepCounter.util

import android.content.SharedPreferences
import android.preference.PreferenceManager

import com.thai.winmyo.stepCounter.R
import com.thai.winmyo.stepCounter.StepApp

/**
 * Created by soelwin on 7/31/16.
 */
object SettingPref {

    private val pref: SharedPreferences
        get() = PreferenceManager.getDefaultSharedPreferences(StepApp.sStepApp.get()!!)


    fun retrieveRecordingInterval(): Int {
        return Integer.parseInt(pref.getString(StepApp.sStepApp.get()!!.getString(R.string.pref_record_interval), "50"))
    }
}
