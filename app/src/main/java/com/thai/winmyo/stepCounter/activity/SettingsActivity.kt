package com.thai.winmyo.stepCounter.activity

import android.os.Bundle
import android.preference.PreferenceActivity

import com.thai.winmyo.stepCounter.R

class SettingsActivity : PreferenceActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.SettingsTheme)
        addPreferencesFromResource(R.xml.preferences)
    }

}
