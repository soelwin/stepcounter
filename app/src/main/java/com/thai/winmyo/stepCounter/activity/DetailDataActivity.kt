package com.thai.winmyo.stepCounter.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener
import com.thai.winmyo.stepCounter.R
import com.thai.winmyo.stepCounter.algorithm.*
import com.thai.winmyo.stepCounter.data.AbsStepCountingAlgorithm
import com.thai.winmyo.stepCounter.data.SaveFile
import com.thai.winmyo.stepCounter.data.SaveFileCache
import com.thai.winmyo.stepCounter.data.getActivityList
import com.thai.winmyo.stepCounter.util.CommonHelper
import com.thai.winmyo.stepCounter.util.OnTabSelected
import com.thai.winmyo.stepCounter.util.onClick
import com.thai.winmyo.stepCounter.util.onTextChanged
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_detail_data.*
import java.util.*

class DetailDataActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener, OnRangeSeekbarFinalValueListener, OnTabSelected {
    private lateinit var mAlgorithm: AbsStepCountingAlgorithm
    private lateinit var mSaveFile: SaveFile

    private fun setupUI() {
        edtInputPersonName.setText(mSaveFile.personName)
        edtInputPersonHeight.setText(mSaveFile.personHeight.toString())
        spPersonActivity.setSelection(mSaveFile.personActivity, true)
        spAlgoPersonActivity.setSelection(mSaveFile.algoPersonActivity, true)
        spPhonePosition.setSelection(mSaveFile.phonePosition, true)
        spAlgoVersion.setSelection(mSaveFile.algoVersion, true)
        tvAlgoStepCount.text = mSaveFile.algoStepCount.toString()
        edtUserStepCount.setText(mSaveFile.userStepCount.toString())
        tvDistance.text = CommonHelper.round(mSaveFile.distance, 2).toString()
        edtInputPointGrouping.setText(mSaveFile.pointGrouping.toString())

        rsHandlingFilter.setMinStartValue(mSaveFile.minHandlingFilter.toFloat()).apply()
        rsHandlingFilter.setMaxStartValue(mSaveFile.maxHandlingFilter.toFloat()).apply()
        tvHandlingFilterMinValue.text = mSaveFile.minHandlingFilter.toString()
        tvHandlingFilterMaxValue.text = mSaveFile.maxHandlingFilter.toString()
    }

    private fun buildArrayAdapter(values: Array<String>): ArrayAdapter<String> {
        val dataAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, Arrays.asList(*values))
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        return dataAdapter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_data)
        mSaveFile = intent.getParcelableExtra(SAVE_FILE)

        spPersonActivity.adapter = buildArrayAdapter(getActivityList().map { it.toString() }.toTypedArray())
        spPersonActivity.onItemSelectedListener = this

        spAlgoPersonActivity.adapter = buildArrayAdapter(getActivityList().map { it.toString() }.toTypedArray())
        spAlgoPersonActivity.onItemSelectedListener = this

        val phonePositions = resources.getStringArray(R.array.phone_positions)
        spPhonePosition.adapter = buildArrayAdapter(phonePositions)
        spPhonePosition.onItemSelectedListener = this

        val algoVersions = resources.getStringArray(R.array.algo_versions)
        spAlgoVersion.adapter = buildArrayAdapter(algoVersions)
        spAlgoVersion.onItemSelectedListener = this

        rsHandlingFilter.setOnRangeSeekbarFinalValueListener(this)
        tabs.addOnTabSelectedListener(this)

        setupUI()
        onTextChanged()
        onClicked()

    }

    private fun buildAlgoFromSaveFile(saveFile: SaveFile): AbsStepCountingAlgorithm {
        return when (saveFile.algoVersion) {
            0 -> StepCountingAlgorithmV1.createInstance(saveFile)
            1 -> StepCountingAlgorithmV2.createInstance(saveFile)
            2 -> StepCountingAlgorithmV3.createInstance(saveFile)
            3 -> StepCountingAlgorithmV4.createInstance(saveFile)
            4 -> StepCountingAlgorithmV5.createInstance(saveFile)
            else -> throw IllegalArgumentException()
        }
    }

    private fun checkStep() {
        pbLoading.visibility = View.VISIBLE
        Completable
                .fromRunnable { mAlgorithm = buildAlgoFromSaveFile(mSaveFile) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    mSaveFile.algoStepCount = mAlgorithm.totalSteps.toInt()
                    mSaveFile.distance = mAlgorithm.distance

                    tvAlgoStepCount.text = mAlgorithm.totalSteps.toString()
                    tvDistance.text = mAlgorithm.distance.toString()
                    pbLoading.visibility = View.GONE
                }
    }

    private fun onTextChanged() {
        edtUserStepCount.onTextChanged {
            if (!TextUtils.isEmpty(it)) {
                mSaveFile.userStepCount = Integer.parseInt(it.toString())
            }
        }
        edtInputPersonName.onTextChanged {
            if (!TextUtils.isEmpty(it)) {
                mSaveFile.personName = it.toString()
            }
        }
        edtInputPersonHeight.onTextChanged {
            if (!TextUtils.isEmpty(it)) {
                mSaveFile.personHeight = java.lang.Double.parseDouble(it.toString())
            }
        }
        edtInputPointGrouping.onTextChanged {
            if (!TextUtils.isEmpty(it)) {
                mSaveFile.pointGrouping = Integer.parseInt(it.toString())
            }
        }
    }

    private fun onClicked() {
        btnSaveData.onClick {
            CommonHelper.doSaving(mSaveFile)
            goBackToMain()
        }
        btnDoStepAlgo.onClick {
            checkStep()
        }
    }

    private fun goBackToMain() {
        SaveFileCache.saveFile = mSaveFile
        finish()
    }

    override fun onBackPressed() {
        goBackToMain()
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        when (parent.id) {
            R.id.spPhonePosition -> mSaveFile.phonePosition = position
            R.id.spPersonActivity -> mSaveFile.personActivity = position
            R.id.spAlgoPersonActivity -> mSaveFile.algoPersonActivity = position
            R.id.spAlgoVersion -> mSaveFile.algoVersion = position
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>) {

    }

    override fun finalValue(minValue: Number, maxValue: Number) {
        mSaveFile.minHandlingFilter = minValue.toInt()
        mSaveFile.maxHandlingFilter = maxValue.toInt()

        tvHandlingFilterMinValue.text = mSaveFile.minHandlingFilter.toString()
        tvHandlingFilterMaxValue.text = mSaveFile.maxHandlingFilter.toString()
    }

    override fun onResume() {
        super.onResume()
        showLayoutByTab(tabs.selectedTabPosition)
    }

    private fun showLayoutByTab(position: Int) {
        when (position) {
            0 -> {
                layoutUserProfile.visibility = View.VISIBLE
                layout_algo_data.visibility = View.GONE
            }
            1 -> {
                layoutUserProfile.visibility = View.GONE
                layout_algo_data.visibility = View.VISIBLE
            }
        }
    }

    override fun onTabSelected(tab: TabLayout.Tab) {
        showLayoutByTab(tab.position)
    }

    companion object {

        private const val SAVE_FILE = "save_file"

        fun buildIntent(context: Context, saveFile: SaveFile): Intent {
            val intent = Intent(context, DetailDataActivity::class.java)
            intent.putExtra(SAVE_FILE, saveFile)
            return intent
        }
    }
}
