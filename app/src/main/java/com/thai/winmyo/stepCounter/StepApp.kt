package com.thai.winmyo.stepCounter

import android.app.Application
import timber.log.Timber
import java.lang.ref.WeakReference

/**
 * Created by sherly on 1/16/18.
 */

class StepApp : Application() {

    override fun onCreate() {
        super.onCreate()
        sStepApp = WeakReference(this)
        Timber.plant(Timber.DebugTree())
    }

    companion object {
        lateinit var sStepApp: WeakReference<StepApp>
    }
}
