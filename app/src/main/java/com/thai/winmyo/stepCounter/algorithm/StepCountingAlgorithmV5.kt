package com.thai.winmyo.stepCounter.algorithm

import com.thai.winmyo.stepCounter.data.AbsSensorDataSource
import com.thai.winmyo.stepCounter.data.AbsStepCountingAlgorithm
import com.thai.winmyo.stepCounter.data.SaveFile
import com.thai.winmyo.stepCounter.data.StepDataNoiseFilter
import com.thai.winmyo.stepCounter.plot.StepSensor
import com.thai.winmyo.stepCounter.util.CommonHelper

/**
 * Created by sherly on 3/18/18.
 */


class StepCountingAlgorithmV5(saveFile: SaveFile) : AbsStepCountingAlgorithm(saveFile) {

    override fun buildDataSource(saveFile: SaveFile): AbsSensorDataSource {
        return SensorDataSourceV5(saveFile)
    }

    override fun checkSteps() {
        mSensorDataSource.getSensorPoints(StepSensor.AC)
                .map { CommonHelper.getRoundAverageValue(it) }
                .toList()
                .let { avList ->
                    var next: Double? = null
                    var prev: Double? = null
                    var current: Double?
                    var peakCount = 0
                    var peakOccurrence = 0
                    var bottomCountBetweenPeak = 0
                    for (i in avList.indices) {
                        current = avList[i]
                        if (i - 1 > 0)
                            prev = avList[i - 1]
                        if (i + 1 < avList.size)
                            next = avList[i + 1]

                        if (next != null && prev != null && current > next && current > prev) {
                            if (peakOccurrence > 2 && bottomCountBetweenPeak == 1)
                                peakCount++
                            peakOccurrence = 0
                            bottomCountBetweenPeak = 0
                        } else {
                            peakOccurrence++
                        }
                        if (next != null && prev != null && current < next && current < prev) {
                            bottomCountBetweenPeak++
                        }
                        prev = null
                        next = prev
                    }
                    totalSteps = peakCount.toLong()

                }
    }

    private class SensorDataSourceV5 internal constructor(saveFile: SaveFile) : AbsSensorDataSource(saveFile) {

        override fun doCustomFilter() {
            StepDataNoiseFilter.lowPassFilter(getSensorPoints(StepSensor.AC))
            StepDataNoiseFilter.highPassFilter(getSensorPoints(StepSensor.AC))
        }
    }

    companion object {

        fun createInstance(saveFile: SaveFile): AbsStepCountingAlgorithm {
            return StepCountingAlgorithmV5(saveFile)
        }
    }
}