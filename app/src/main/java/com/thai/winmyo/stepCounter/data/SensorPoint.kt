package com.thai.winmyo.stepCounter.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by soelwin on 7/24/16.
 */
@Parcelize
class SensorPoint(var x: Float, var y: Float, var z: Float, var timestamp: Long, var diffTime: Long) : Parcelable

fun FloatArray.buildFromSensorValue(timestamp: Long, diffTime: Long): SensorPoint {
    return SensorPoint(this[0], this[1], this[2], timestamp, diffTime)
}
