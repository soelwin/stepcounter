package com.thai.winmyo.stepCounter.plot

/**
 * Created by sherly on 1/18/18.
 */

enum class SensorDataType {
    RAW, FILTER
}
