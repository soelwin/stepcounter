package com.thai.winmyo.stepCounter.data

enum class PersonActivity {
    Laying,
    Sitting,
    Standing,
    NormalWalk,
    StandWalk,
    Downstairs,
    Upstairs,
    Jogging,
    None
}

fun getActivityList(): List<PersonActivity> {
    return listOf(
            PersonActivity.Laying,
            PersonActivity.Sitting,
            PersonActivity.Standing,
            PersonActivity.NormalWalk,
            PersonActivity.StandWalk,
            PersonActivity.Downstairs,
            PersonActivity.Upstairs,
            PersonActivity.Jogging,
            PersonActivity.None
    )
}