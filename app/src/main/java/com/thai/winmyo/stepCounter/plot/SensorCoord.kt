package com.thai.winmyo.stepCounter.plot

import android.graphics.Color
import android.support.annotation.ColorInt

/**
 * Created by sherly on 1/17/18.
 */


enum class SensorCoord {
    X, Y, Z;

    val color: Int
        @ColorInt
        get() {
            return when (this) {
                X -> Color.RED
                Y -> Color.GREEN
                Z -> Color.BLUE
            }
        }
}

