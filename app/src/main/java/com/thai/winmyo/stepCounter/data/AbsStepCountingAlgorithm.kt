package com.thai.winmyo.stepCounter.data

import com.thai.winmyo.stepCounter.plot.StepSensor
import com.thai.winmyo.stepCounter.util.CommonHelper
import kotlin.math.roundToInt

/**
 * Created by sherly on 3/7/18.
 */

abstract class AbsStepCountingAlgorithm(private var mSaveFile: SaveFile) {
    var totalSteps: Long = 0
    var distance = 0.0
    var activity: PersonActivity = PersonActivity.None
    protected var mSensorDataSource: AbsSensorDataSource

    init {
        mSensorDataSource = buildDataSource(mSaveFile)
        mSensorDataSource.doNoiseFilter()
        checkSteps()
        if (mSensorDataSource.isValidForCombination()) {
            calculateActivity(doCombination())
        }
    }

    private fun doCombination(): CheckActivityData {
        val agList = combineTwoSensorList(mSensorDataSource.getSensorPoints(StepSensor.AC), mSensorDataSource.getSensorPoints(StepSensor.GY))
        val amList = combineTwoSensorList(mSensorDataSource.getSensorPoints(StepSensor.AC), mSensorDataSource.getSensorPoints(StepSensor.MG))
        val agmList = combineThreeSensorList(mSensorDataSource.getSensorPoints(StepSensor.AC), mSensorDataSource.getSensorPoints(StepSensor.GY), mSensorDataSource.getSensorPoints(StepSensor.MG))

        mSaveFile.getCombineSensorPoints(StepSensor.AC).addAll(agmList)
        mSaveFile.getCombineSensorPoints(StepSensor.MG).addAll(amList)
        mSaveFile.getCombineSensorPoints(StepSensor.GY).addAll(agList)

        val agmThreshold = agmList.calculateAverageMean().roundToInt()
        val agThreshold = agList.calculateAverageMean().roundToInt()
        val amThreshold = amList.calculateAverageMean().roundToInt()
        return CheckActivityData(totalSteps.toInt(), agmThreshold, agThreshold, amThreshold)
    }

    fun checkSteps(stepSensor: StepSensor) {
        mSensorDataSource.getSensorPoints(stepSensor)
                .map { CommonHelper.getDisplacement(it) }
                .toList()
                .let {
                    distance = 0.0
                    for (d in it) {
                        distance += d
                    }
                    distance = distance * 3.0 / mSaveFile.personHeight
                    totalSteps = Math.round(distance)
                }
    }

    abstract fun buildDataSource(saveFile: SaveFile): AbsSensorDataSource

    abstract fun checkSteps()
}
