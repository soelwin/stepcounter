package com.thai.winmyo.stepCounter.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.DashPathEffect
import android.graphics.Paint
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.os.Environment
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.androidplot.util.PixelUtils
import com.androidplot.xy.*
import com.nbsp.materialfilepicker.MaterialFilePicker
import com.nbsp.materialfilepicker.ui.FilePickerActivity
import com.thai.winmyo.stepCounter.R
import com.thai.winmyo.stepCounter.data.*
import com.thai.winmyo.stepCounter.plot.AppXYDataSource
import com.thai.winmyo.stepCounter.plot.AppXYSeries
import com.thai.winmyo.stepCounter.plot.SensorCoord
import com.thai.winmyo.stepCounter.plot.StepSensor
import com.thai.winmyo.stepCounter.util.CommonHelper
import com.thai.winmyo.stepCounter.util.onClick
import kotlinx.android.synthetic.main.activity_record_data.*
import java.io.File
import java.text.DecimalFormat
import java.util.regex.Pattern

class RecordDataActivity : AppCompatActivity(), SensorEventListener {

    private lateinit var accXDataSource: AppXYDataSource
    private lateinit var accYDataSource: AppXYDataSource
    private lateinit var accZDataSource: AppXYDataSource
    private lateinit var gyroXDataSource: AppXYDataSource
    private lateinit var gyroYDataSource: AppXYDataSource
    private lateinit var gyroZDataSource: AppXYDataSource
    private lateinit var magnetoXDataSource: AppXYDataSource
    private lateinit var magnetoYDataSource: AppXYDataSource
    private lateinit var magnetoZDataSource: AppXYDataSource

    private var sensorManager: SensorManager? = null
    private var accelerometer: Sensor? = null
    private var gyroscope: Sensor? = null
    private var magneto: Sensor? = null


    lateinit var mSaveFile: SaveFile

    override fun onDestroy() {
        super.onDestroy()
        stopSensorListening()
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_record_data)
        setUpSensor()
        buildAccPlot()
        buildGyroPlot()
        buildMagnetoPlot()

        btn_start_sensing.onClick {
            btn_start_sensing!!.visibility = View.INVISIBLE
            btn_stop_sensing!!.visibility = View.VISIBLE
            startSensorListening()
        }
        btn_stop_sensing.onClick {
            stopSensorListening()
            btn_start_sensing!!.visibility = View.VISIBLE
            btn_stop_sensing!!.visibility = View.INVISIBLE
            cleanDataAfterStop()
            goToDetail()
        }

        fab_setting.onClick {
            goToSetting()
        }

        fab_choose_file.onClick {
            val mediaStorageDir = File(Environment.getExternalStorageDirectory().toString() + "/WinStepCounter/")
            MaterialFilePicker()
                    .withActivity(this)
                    .withRequestCode(REQUEST_CODE_FILE_CHOOSE)
                    .withPath(mediaStorageDir.path)
                    .withFilter(Pattern.compile(".*\\.txt$")) // Filtering files and directories by file name using regexp
                    .withFilterDirectories(true) // Set directories filterable (false by default)
                    .withHiddenFiles(true) // Show hidden files and folders
                    .start()
        }

    }

    private fun goToDetail() {
        val intent = DetailDataActivity.buildIntent(this, mSaveFile)
        startActivity(intent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_CODE_FILE_CHOOSE -> if (resultCode == Activity.RESULT_OK) {
                val filePath = data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH)
                mSaveFile = CommonHelper.convertToSaveFile(filePath)
                goToDetail()
            }
        }
    }

    private fun setUpSensor() {
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        if (sensorManager != null) {
            accelerometer = sensorManager!!.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
            gyroscope = sensorManager!!.getDefaultSensor(Sensor.TYPE_GYROSCOPE)
            if (gyroscope == null) {
                plot_gyro!!.visibility = View.INVISIBLE
            }
            magneto = sensorManager!!.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)
            if (magneto == null) {
                plot_magneto!!.visibility = View.INVISIBLE
            }
        }
    }

    private fun startSensorListening() {
        mSaveFile = SaveFileCache.saveFile
        sensorManager!!.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_FASTEST)
        sensorManager!!.registerListener(this, gyroscope, SensorManager.SENSOR_DELAY_FASTEST)
        sensorManager!!.registerListener(this, magneto, SensorManager.SENSOR_DELAY_FASTEST)
    }

    private fun stopSensorListening() {
        sensorManager!!.unregisterListener(this, accelerometer)
        sensorManager!!.unregisterListener(this, gyroscope)
        sensorManager!!.unregisterListener(this, magneto)
    }

    private fun buildAccPlot() {

        accXDataSource = AppXYDataSource()
        setUpLineForPlot(plot_acc!!, accXDataSource, StepSensor.AC, SensorCoord.X)

        accYDataSource = AppXYDataSource()
        setUpLineForPlot(plot_acc!!, accYDataSource, StepSensor.AC, SensorCoord.Y)

        accZDataSource = AppXYDataSource()
        setUpLineForPlot(plot_acc!!, accZDataSource, StepSensor.AC, SensorCoord.Z)

        setUpGraphForPlot(plot_acc!!, 2.0, 10.0, 20.0)
    }

    private fun buildGyroPlot() {

        gyroXDataSource = AppXYDataSource()
        setUpLineForPlot(plot_gyro!!, gyroXDataSource, StepSensor.GY, SensorCoord.X)

        gyroYDataSource = AppXYDataSource()
        setUpLineForPlot(plot_gyro!!, gyroYDataSource, StepSensor.GY, SensorCoord.Y)

        gyroZDataSource = AppXYDataSource()
        setUpLineForPlot(plot_gyro!!, gyroZDataSource, StepSensor.GY, SensorCoord.Z)

        setUpGraphForPlot(plot_gyro!!, 2.0, 25.0, 50.0)
    }

    private fun buildMagnetoPlot() {

        magnetoXDataSource = AppXYDataSource()
        setUpLineForPlot(plot_magneto!!, magnetoXDataSource, StepSensor.MG, SensorCoord.X)

        magnetoYDataSource = AppXYDataSource()
        setUpLineForPlot(plot_magneto!!, magnetoYDataSource, StepSensor.MG, SensorCoord.Y)

        magnetoZDataSource = AppXYDataSource()
        setUpLineForPlot(plot_magneto!!, magnetoZDataSource, StepSensor.MG, SensorCoord.Z)

        setUpGraphForPlot(plot_magneto!!, 2.0, 25.0, 50.0)
    }

    private fun cleanDataAfterStop() {
        accXDataSource.resetData()
        accYDataSource.resetData()
        accZDataSource.resetData()
        plot_acc!!.redraw()

        gyroXDataSource.resetData()
        gyroYDataSource.resetData()
        gyroZDataSource.resetData()
        plot_gyro!!.redraw()

        magnetoXDataSource.resetData()
        magnetoYDataSource.resetData()
        magnetoZDataSource.resetData()
        plot_magneto!!.redraw()

    }

    private fun setUpGraphForPlot(xyPlot: XYPlot, rowValue: Double, columnValue: Double, count: Double) {
        xyPlot.graph.getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).format = DecimalFormat("0")
        xyPlot.domainStepMode = StepMode.INCREMENT_BY_VAL
        xyPlot.domainStepValue = rowValue

        xyPlot.legend.isDrawIconBackgroundEnabled = false
        xyPlot.rangeStepMode = StepMode.INCREMENT_BY_VAL
        xyPlot.rangeStepValue = columnValue

        xyPlot.graph.getLineLabelStyle(XYGraphWidget.Edge.LEFT).format = DecimalFormat("###.#")

        xyPlot.setRangeBoundaries(-count, count, BoundaryMode.FIXED)

        val dashFx = DashPathEffect(floatArrayOf(PixelUtils.dpToPix(3f), PixelUtils.dpToPix(3f)), 0f)
        xyPlot.graph.domainGridLinePaint.pathEffect = dashFx
        xyPlot.graph.rangeGridLinePaint.pathEffect = dashFx
    }

    private fun setUpLineForPlot(xyPlot: XYPlot, dataSource: AppXYDataSource, stepSensor: StepSensor, sensorCoord: SensorCoord) {
        val dynamicSeries = AppXYSeries(dataSource, stepSensor.toString() + " " + sensorCoord)

        val formatter = LineAndPointFormatter(sensorCoord.color, null, null, null)
        formatter.linePaint.strokeJoin = Paint.Join.ROUND
        formatter.linePaint.strokeWidth = 4f

        xyPlot.addSeries(dynamicSeries, formatter)
    }

    override fun onSensorChanged(sensorEvent: SensorEvent) {
        val currentTimeInMillis = System.currentTimeMillis()
        val stepSensor = AbsSensorDataSource.convertSensorTypeToStepSensor(sensorEvent.sensor.type)
        val diffTime = calculateDiffTime(currentTimeInMillis, mSaveFile.getLastRawPointTimeStamp(stepSensor))
        if (diffTime > -1) {
            when (sensorEvent.sensor.type) {
                Sensor.TYPE_ACCELEROMETER -> {
                    accXDataSource.appendDataToLast(sensorEvent.values[0].toDouble())
                    accYDataSource.appendDataToLast(sensorEvent.values[1].toDouble())
                    accZDataSource.appendDataToLast(sensorEvent.values[2].toDouble())
                    plot_acc!!.redraw()
                }
                Sensor.TYPE_GYROSCOPE -> {
                    gyroXDataSource.appendDataToLast(sensorEvent.values[0].toDouble())
                    gyroYDataSource.appendDataToLast(sensorEvent.values[1].toDouble())
                    gyroZDataSource.appendDataToLast(sensorEvent.values[2].toDouble())
                    plot_gyro!!.redraw()
                }
                Sensor.TYPE_MAGNETIC_FIELD -> {
                    magnetoXDataSource.appendDataToLast(sensorEvent.values[0].toDouble())
                    magnetoYDataSource.appendDataToLast(sensorEvent.values[1].toDouble())
                    magnetoZDataSource.appendDataToLast(sensorEvent.values[2].toDouble())
                    plot_magneto!!.redraw()

                }
            }
            mSaveFile.addSensorPoint(stepSensor, sensorEvent.values.buildFromSensorValue(currentTimeInMillis, diffTime))

        }
    }

    private fun calculateDiffTime(current: Long, lastUpdate: Long): Long {
        if (lastUpdate == 0L) {
            return 0
        }
        val diff = current - lastUpdate
        return if (CommonHelper.isValidTime(diff)) {
            diff
        } else {
            -1
        }
    }

    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {

    }

    private fun goToSetting() {
        val modifySettings = Intent(this, SettingsActivity::class.java)
        startActivity(modifySettings)
    }

    companion object {
        private const val REQUEST_CODE_FILE_CHOOSE = 123
    }

}