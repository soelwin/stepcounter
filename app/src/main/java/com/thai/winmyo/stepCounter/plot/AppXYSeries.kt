package com.thai.winmyo.stepCounter.plot

import com.androidplot.xy.XYSeries

/**
 * Created by sherly on 1/17/18.
 */

class AppXYSeries(private val datasource: AppXYDataSource, private val title: String) : XYSeries {

    override fun getTitle(): String {
        return title
    }

    override fun size(): Int {
        return datasource.itemCount
    }

    override fun getX(index: Int): Number {
        return datasource.getX(index)
    }

    override fun getY(index: Int): Number {
        return datasource.getY(index)
    }
}