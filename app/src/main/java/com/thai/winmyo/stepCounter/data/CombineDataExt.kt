package com.thai.winmyo.stepCounter.data

import com.thai.winmyo.stepCounter.data.ThresholdConstant.TOTAL_STEP_FILTER


fun List<Any>.isSizeEqual(list: List<Any>) = isNotEmpty() and list.isNotEmpty() and (size == list.size)

fun calculateCombineFormula(acI: Double, mgI: Double, vaI: Double, vbI: Double): Double {
    return acI + (vaI.combineVariance(vbI) * (mgI - acI))
}

fun Double.combineVariance(vbI: Double) = this / (this + vbI)

fun List<Float>.getVariance(index: Int): Double {
    return getSigma(index).divideBySample(size)
}

fun Double.divideBySample(sampleSize: Int) = this / (sampleSize - 1.0)


fun List<Float>.getSigma(index: Int): Double {
    var total = 0.0
    for (i in withIndex()) {
        if (i.index <= index)
            total += Math.pow(i.value + calculateMean(), 2.0)
        else
            return total
    }
    return total
}

fun List<Float>.calculateMean(): Double {
    var total = 0.0f
    for (i in this) {
        total += i
    }
    return total / this.size.toDouble()
}

fun calculateActivity(data: CheckActivityData): PersonActivity {
    if (data.totalStep == TOTAL_STEP_FILTER) {
        if (data.agmThreshold < ThresholdConstant.AC_THRESHOLD_LAYING_FILTER) {
            return PersonActivity.Laying
        } else {
            if (data.agmThreshold < ThresholdConstant.AC_THRESHOLD_SITTING_FILTER) {
                return PersonActivity.Sitting
            } else {
                return PersonActivity.Standing
            }
        }
    } else {
        if (data.agThreshold == ThresholdConstant.GY_THRESHOLD_NORMAL_WALK_FILTER) {
            return PersonActivity.NormalWalk
        } else {
            if (data.amThreshold == ThresholdConstant.MG_THRESHOLD_STAND_WALK_FILTER) {
                return PersonActivity.StandWalk
            } else {
                if (data.agThreshold > data.amThreshold) {
                    if (data.agThreshold > ThresholdConstant.GY_THRESHOLD_DOWNSTAIR_WALK_FILTER) {
                        return PersonActivity.Downstairs
                    } else {
                        return PersonActivity.Upstairs
                    }
                } else {
                    return PersonActivity.Jogging
                }
            }
        }
    }
}

data class CheckActivityData(
        val totalStep: Int,
        val agmThreshold: Int,
        val agThreshold: Int,
        val amThreshold: Int
)