package com.thai.winmyo.stepCounter.algorithm

import com.thai.winmyo.stepCounter.data.AbsSensorDataSource
import com.thai.winmyo.stepCounter.data.AbsStepCountingAlgorithm
import com.thai.winmyo.stepCounter.data.SaveFile
import com.thai.winmyo.stepCounter.data.StepDataNoiseFilter
import com.thai.winmyo.stepCounter.plot.StepSensor

/**
 * Created by sherly on 1/20/18.
 */

class StepCountingAlgorithmV1 private constructor(saveFile: SaveFile) : AbsStepCountingAlgorithm(saveFile) {

    override fun buildDataSource(saveFile: SaveFile): AbsSensorDataSource {
        return SensorDataSourceV1(saveFile)
    }

    override fun checkSteps() {
        checkSteps(StepSensor.AC)
    }

    private class SensorDataSourceV1 internal constructor(saveFile: SaveFile) : AbsSensorDataSource(saveFile) {

        override fun doCustomFilter() {
            doDefaultFilter()
            StepDataNoiseFilter.handlingFilter(getSensorPoints(StepSensor.AC), saveFile)
        }
    }

    companion object {

        fun createInstance(saveFile: SaveFile): AbsStepCountingAlgorithm {
            return StepCountingAlgorithmV1(saveFile)
        }
    }

}