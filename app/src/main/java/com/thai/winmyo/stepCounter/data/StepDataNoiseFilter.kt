package com.thai.winmyo.stepCounter.data

import android.util.SparseArray
import com.thai.winmyo.stepCounter.util.CommonHelper
import io.reactivex.Observable
import java.util.*

/**
 * Created by sherly on 1/19/18.
 */

object StepDataNoiseFilter {
    private val ALPHA_LOW_PASS_FILTER = 0.2f

    private val ALPHA_HIGH_PASS_FILTER = 0.8f

    fun highPassFilter(sensorPoints: MutableList<SensorPoint>) {
        for (current in sensorPoints) {
            current.x = current.x - (1 - ALPHA_HIGH_PASS_FILTER) * current.x
            current.y = current.y - (1 - ALPHA_HIGH_PASS_FILTER) * current.y
            current.z = current.z - (1 - ALPHA_HIGH_PASS_FILTER) * current.z
        }
    }

    fun lowPassFilter(sensorPoints: MutableList<SensorPoint>) {
        for (i in sensorPoints.indices) {
            val current: SensorPoint = sensorPoints[i]
            var prev: SensorPoint? = null
            if (i > 0)
                prev = sensorPoints[i - 1]
            if (prev != null) {
                current.x = prev.x + ALPHA_LOW_PASS_FILTER * (current.x - prev.x)
            }
        }
    }

    fun smoothNoise(sensorPoints: MutableList<SensorPoint>) {
        val w1 = 0.5f
        val w2 = 0.3f
        val w3 = 0.2f
        for (i in sensorPoints.indices) {
            var x = sensorPoints[i].x
            if (i >= 2) {
                x = w1 * x + w2 * sensorPoints[i - 1].x + w3 * sensorPoints[i - 2].x
                sensorPoints[i].x = x
            }

            var y = sensorPoints[i].y
            if (i >= 2) {
                y = w1 * y + w2 * sensorPoints[i - 1].y + w3 * sensorPoints[i - 2].y
                sensorPoints[i].y = y
            }

            var z = sensorPoints[i].z
            if (i >= 2) {
                z = w1 * z + w2 * sensorPoints[i - 1].z + w3 * sensorPoints[i - 2].z
                sensorPoints[i].z = z
            }
        }
    }

    fun handlingFilter(sensorPoints: MutableList<SensorPoint>, saveFile: SaveFile) {
        var currentPoints: List<SensorPoint>
        var prevPoints: List<SensorPoint>
        var nextPoints: List<SensorPoint>
        val lists = CommonHelper.getPointsByGroup(sensorPoints, saveFile.pointGrouping)

        val sparseArray = SparseArray<List<SensorPoint>>()
        val handlingGroup = ArrayList<Int>()
        for (i in lists.indices) {
            prevPoints = ArrayList()
            nextPoints = ArrayList()
            currentPoints = lists[i]
            if (i > 0) prevPoints = lists[i - 1]
            if (i + 1 < lists.size) nextPoints = lists[i + 1]

            val sensorPointList = ArrayList<SensorPoint>()
            sensorPointList.addAll(prevPoints)
            sensorPointList.addAll(currentPoints)
            sensorPointList.addAll(nextPoints)

            Observable.fromIterable(sensorPointList)
                    .map { CommonHelper.getAverageValue(it) }
                    .toList()
                    .map { averageValues ->
                        var mode = 0
                        for (av in averageValues) {
                            if (CommonHelper.frequency(averageValues, av!!) > 1) {
                                mode++
                            }
                        }
                        CommonHelper.getPercentage(averageValues.size.toDouble(), mode.toDouble())
                    }
                    .filter { percentage -> percentage < saveFile.minHandlingFilter || percentage > saveFile.maxHandlingFilter }
                    .subscribe { handlingGroup.add(i) }

            sparseArray.put(i, currentPoints)
        }


        for (handlingIndex in handlingGroup) {
            sparseArray.remove(handlingIndex)
        }

        if (handlingGroup.size > 0) {
            sensorPoints.clear()
            sensorPoints.addAll(CommonHelper.convertToList(sparseArray)!!)
        }
    }

    fun peekFilter(sensorPoints: MutableList<SensorPoint>, saveFile: SaveFile) {
        val lists = CommonHelper.getPointsByGroup(sensorPoints, saveFile.pointGrouping)

        val sparseArray = SparseArray<List<SensorPoint>>()

        val handlingGroup = ArrayList<Int>()
        for (i in lists.indices) {
            val points = lists[i]
            var mLastAV: Double? = null

            var upDownPatterns = 0
            var upDownMovements = 0
            for (sensorPoint in points) {
                val av = CommonHelper.getAverageValue(sensorPoint)
                if (mLastAV == null) {
                    mLastAV = av
                } else {
                    val deltaAV = CommonHelper.round(mLastAV, 1) - CommonHelper.round(av, 1)
                    mLastAV = av

                    if (deltaAV < 0) {
                        if (upDownMovements != 1) {
                            ++upDownMovements
                        }

                        if (upDownMovements == 0) {
                            upDownPatterns++
                        }
                    }
                    if (deltaAV > 0) {
                        if (upDownMovements != -1) {
                            --upDownMovements
                        }
                        if (upDownMovements == 0) {
                            upDownPatterns++
                        }
                    }
                }
            }
            if (upDownPatterns < 1)
                handlingGroup.add(i)
            sparseArray.put(i, points)
        }


        for (handlingIndex in handlingGroup) {
            sparseArray.remove(handlingIndex)
        }

        if (handlingGroup.size > 0) {
            sensorPoints.clear()
            sensorPoints.addAll(CommonHelper.convertToList(sparseArray)!!)
        }
    }
}
