package com.thai.winmyo.stepCounter.data

import android.os.Parcelable
import com.thai.winmyo.stepCounter.plot.StepSensor
import kotlinx.android.parcel.Parcelize
import java.text.DateFormat
import java.util.*

/**
 * Created by sherly on 2/28/18.
 */

@Parcelize
data class SaveFile(
        var timestamp: String = DateFormat.getDateTimeInstance().format(Calendar.getInstance().time),
        var personName: String = "",
        var personActivity: Int = 0,
        var algoPersonActivity: Int = 0,
        var algoStepCount: Int = 0,
        var userStepCount: Int = 0,
        var phonePosition: Int = 0,
        var algoVersion: Int = 0,
        var distance: Double = 0.0,
        var personHeight: Double = 1.68,
        var pointGrouping: Int = 10,
        var minHandlingFilter: Int = 30,
        var maxHandlingFilter: Int = 95,
        var acRawPoints: MutableList<SensorPoint> = mutableListOf(),
        var gyRawPoints: MutableList<SensorPoint> = mutableListOf(),
        var mgRawPoints: MutableList<SensorPoint> = mutableListOf(),
        var agmCombinePoints: MutableList<SensorPoint> = mutableListOf(),
        var agCombinePoints: MutableList<SensorPoint> = mutableListOf(),
        var amCombinePoints: MutableList<SensorPoint> = mutableListOf()
) : Parcelable

fun SaveFile.addSensorPoint(stepSensor: StepSensor, sensorPoint: SensorPoint) {
    getSensorPoints(stepSensor).add(sensorPoint)
}

fun SaveFile.getSensorPoints(stepSensor: StepSensor): MutableList<SensorPoint> {
    return when (stepSensor) {
        StepSensor.AC -> acRawPoints
        StepSensor.MG -> mgRawPoints
        StepSensor.GY -> gyRawPoints
    }
}


fun SaveFile.getCombineSensorPoints(stepSensor: StepSensor): MutableList<SensorPoint> {
    return when (stepSensor) {
        StepSensor.AC -> agmCombinePoints
        StepSensor.MG -> amCombinePoints
        StepSensor.GY -> agCombinePoints
    }
}

fun SaveFile.getLastRawPointTimeStamp(stepSensor: StepSensor): Long {
    val rawPoint = getSensorPoints(stepSensor)
    return if (rawPoint.isEmpty()) {
        0
    } else {
        rawPoint[rawPoint.size - 1].timestamp
    }
}

fun SaveFile.buildFileName() = "$timestamp.txt"
