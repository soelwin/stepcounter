package com.thai.winmyo.stepCounter.data

object ThresholdConstant {
    const val TOTAL_STEP_FILTER = 0
    const val AC_THRESHOLD_LAYING_FILTER = 9
    const val AC_THRESHOLD_SITTING_FILTER = 5

    const val GY_THRESHOLD_NORMAL_WALK_FILTER = 7
    const val GY_THRESHOLD_DOWNSTAIR_WALK_FILTER = 30


    const val MG_THRESHOLD_STAND_WALK_FILTER = 5
}

fun combineList(listA: List<Float>, listB: List<Float>): List<Float> {
    val combineList = mutableListOf<Float>()
    if (listA.isSizeEqual(listB)) {
        for (ac in listA.withIndex()) {
            val i = ac.index

            val acI = ac.value.toDouble()
            val mgI = listB[i].toDouble()
            val vaI = listA.getVariance(i)
            val vbI = listA.getVariance(i)

            combineList.add(calculateCombineFormula(acI, mgI, vaI, vbI).toFloat())
        }
    }
    return combineList.toList()
}

fun combineTwoSensorList(listA: List<SensorPoint>, listB: List<SensorPoint>): List<SensorPoint> {
    val combineXList = combineList(listA.map { it.x }, listB.map { it.x })
    val combineYList = combineList(listA.map { it.y }, listB.map { it.y })
    val combineZList = combineList(listA.map { it.z }, listB.map { it.z })

    val combineList = mutableListOf<SensorPoint>()
    for (i in listA.withIndex()) {
        combineList.add(SensorPoint(combineXList[i.index], combineYList[i.index], combineZList[i.index], listA[i.index].timestamp, listA[i.index].diffTime))
    }
    return combineList
}

fun combineThreeSensorList(listA: List<SensorPoint>, listB: List<SensorPoint>, listC: List<SensorPoint>): List<SensorPoint> {
    val combineABList = combineTwoSensorList(listA, listB)
    return combineTwoSensorList(combineABList, listC)
}

fun List<SensorPoint>.calculateAverageMean(): Double {
    return this.map { it.getAverageValue().toFloat() }.calculateMean()
}

fun SensorPoint.getAverageValue(): Double {
    return Math.sqrt(Math.pow(x.toDouble(), 2.0) + Math.pow(y.toDouble(), 2.0) + Math.pow(z.toDouble(), 2.0))
}