package com.thai.winmyo.stepCounter.data

import android.hardware.Sensor
import com.thai.winmyo.stepCounter.plot.StepSensor

/**
 * Created by sherly on 1/18/18.
 */

abstract class AbsSensorDataSource(val saveFile: SaveFile) {
    private var mFilterPointsAC: MutableList<SensorPoint> = saveFile.acRawPoints.toTypedArray().toList().toMutableList()
    private var mFilterPointsGY: MutableList<SensorPoint> = saveFile.gyRawPoints.toTypedArray().toList().toMutableList()
    private var mFilterPointsMG: MutableList<SensorPoint> = saveFile.mgRawPoints.toTypedArray().toList().toMutableList()

    abstract fun doCustomFilter()

    protected fun doDefaultFilter() {
        StepDataNoiseFilter.highPassFilter(getSensorPoints(StepSensor.AC))
        StepDataNoiseFilter.smoothNoise(getSensorPoints(StepSensor.AC))

        StepDataNoiseFilter.highPassFilter(getSensorPoints(StepSensor.GY))
        StepDataNoiseFilter.highPassFilter(getSensorPoints(StepSensor.MG))
    }

    fun doNoiseFilter() {
        doCustomFilter()
    }

    fun getSensorPoints(stepSensor: StepSensor): MutableList<SensorPoint> {
        return when (stepSensor) {
            StepSensor.AC -> mFilterPointsAC
            StepSensor.MG -> mFilterPointsMG
            StepSensor.GY -> mFilterPointsGY
        }
    }

    companion object {
        fun convertSensorTypeToStepSensor(type: Int): StepSensor {
            return when (type) {
                Sensor.TYPE_ACCELEROMETER -> StepSensor.AC
                Sensor.TYPE_GYROSCOPE -> StepSensor.GY
                Sensor.TYPE_MAGNETIC_FIELD -> StepSensor.MG
                else -> throw IllegalArgumentException()
            }
        }
    }
}

fun AbsSensorDataSource.isValidForCombination(): Boolean {
    return getSensorPoints(StepSensor.AC).isNotEmpty() and
            getSensorPoints(StepSensor.MG).isNotEmpty() and
            getSensorPoints(StepSensor.GY).isNotEmpty() and
            (getSensorPoints(StepSensor.AC).size == getSensorPoints(StepSensor.MG).size) and
            (getSensorPoints(StepSensor.MG).size == getSensorPoints(StepSensor.GY).size)
}